// Written by Jeremie Kull
// on 11-3-2018
// A program to order 3 numbers

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  int num0;
  printf("Enter integer: ");
  scanf("%d", &num0);
  int num1;
  printf("Enter integer: ");
  scanf("%d", &num1);
  int num2;
  printf("Enter integer: ");
  scanf("%d", &num2);

  if (num0 < num1) {
    if (num0 < num2) {
      printf("The integers in order are: %d ", num0);
      if (num1 < num2) {
        printf("%d %d\n", num1, num2);
      } else {
        printf("%d %d\n", num2, num1);
      }
    } else {
      printf("The integers in order are: %d %d %d\n", num2, num0, num1);
    }
  } else {
    if (num1 < num2) {
      printf("The integers in order are: %d ", num1);
      if (num0 < num2) {
        printf("%d %d\n", num0, num2);
      } else {
        printf("%d %d\n", num2, num0);
      }
    } else {
      printf("The integers in order are: %d %d %d\n", num2, num1, num0);
    }
  }
  return 0;
}
