// Written by Jeremie Kull
// on 11-3-2018
// A function to tell me if a year is a leap year

#include <stdlib.h>
#include <stdio.h>

int isLeapYear(int year) {
  return (int)((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
}

int main(void) {
  int year;
  printf("Enter year: ");
  scanf("%d", &year);

  if (isLeapYear(year)) {
    printf("%d is a leap year.\n", year);
  } else {
    printf("%d is not a leap year.\n", year);
  }

  return 0;
}
