// Written by Jeremie Kull
// on 11-3-2018
// A program to order 3 numbers without if statements

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  int num0;
  printf("Enter integer: ");
  scanf("%d", &num0);
  int num1;
  printf("Enter integer: ");
  scanf("%d", &num1);
  int num2;
  printf("Enter integer: ");
  scanf("%d", &num2);

  int test00 = ((num0 < num1) && (num0 < num2));
  int test01 = ((num1 <= num0) && (num1 < num2));
  int test02 = ((num2 <= num1) && (num2 <= num0));

  int test10 = ((test01 && (num0 < num2)) || (test02 && (num0 < num1)));
  int test11 = ((test00 && (num1 <= num2)) || (test02 && (num1 <= num0)));
  int test12 = ((test00 && (num2 < num1)) || (test01 && (num2 <= num0)));

  int test20 = ((num0 > num1) && (num0 > num2));
  int test21 = ((num1 >= num0) && (num1 > num2));
  int test22 = ((num2 >= num1) && (num2 >= num0));

  printf("The integers in order are: %d %d %d\n", num0*test00 + num1*test01 + num2*test02,
                                                  num0*test10 + num1*test11 + num2*test12,
                                                  num0*test20 + num1*test21 + num2*test22);
  return 0;
}
