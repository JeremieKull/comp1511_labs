// Written by Jeremie Kull
// on 11-3-2018
// A program to order 3 numbers without if statements and only 3 variables

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  int num0;
  printf("Enter integer: ");
  scanf("%d", &num0);
  int num1;
  printf("Enter integer: ");
  scanf("%d", &num1);
  int num2;
  printf("Enter integer: ");
  scanf("%d", &num2);

  printf("The integers in order are: %d %d %d\n",
    num0*((num0 < num1) && (num0 < num2)) + num1*((num1 <= num0) && (num1 < num2)) + num2*((num2 <= num1) && (num2 <= num0)),
    num0*(((num1 < num0) && (num0 < num2)) || ((num2 < num0) && (num0 < num1)))
    + num1*(((num0 <= num1) && (num1 <= num2)) || ((num2 <= num1) && (num1 <= num0)))
    + num2*(((num0 <= num2) && (num2 < num1)) || ((num1 < num2) && (num2 <= num0))),
    num0*((num0 > num1) && (num0 > num2)) + num1*((num1 >= num0) && (num1 > num2)) + num2*((num2 >= num1) && (num2 >= num0)));
  return 0;
}
