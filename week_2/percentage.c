// Written by Jeremie Kull
// on 11-3-2018
// A program to give a percentage mark

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  printf("Enter the total number of marks in the exam: ");
  float total_mark;
  scanf("%f", &total_mark);

  printf("Enter the number of marks the student was awarded: ");
  float mark;
  scanf("%f", &mark);

  printf("The student scored %3.0f%% in this exam.\n", mark/total_mark*100.0);

  return 0;
}
