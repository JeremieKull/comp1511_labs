// Written by Jeremie Kull
// on 18-3-2018
// A program to countdown from 10
// 17monsitar

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  int count = 1;
  printf("Enter number: ");
  int max;
  scanf("%d", &max);
  while (max > count) {
    if (!(count % 3) || !(count % 5)) {
      printf("%d\n", count);
    }
    count++;
  }

  return 0;
}
