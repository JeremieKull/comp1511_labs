// Written by Jeremie Kull
// on 18-3-2018
// A program to concentric boxes
// 17monsitar

#include <math.h>
#include <stdio.h>

int is_hash(int x, int y, int n) {
  int count = 0;
  while (n > count) {
    if (!(x > count && x < n - count  && y > count && y < n - count)) {
      if (count % 2) {
        return 0;
      }
      return 1;
    }
    count++;
  }
  return 0;
}

int main(void) {
  printf("How many boxes: ");
  int boxes;
  scanf("%d", &boxes);
  int size = boxes*4 - 1;


  int y = 0;
  while (y < size) {
    int x = 0;
    while (x < size) {
      if (is_hash(x, y, size - 1)) {
        printf("#");
      } else {
        printf(" ");
      }
      x++;
    }
    y++;
    printf("\n");
  }
  if (!boxes) {
    printf(" \n");
  }

  return 0;
}
