// Written by Jeremie Kull
// on 18-3-2018
// A program to write a spiral
// 17monsitar

#include <stdio.h>

int main(void) {
  int n;
  printf("Enter size: ");
  scanf("%d", &n);

  int N = (n-1)/2;
  int total_n = n+ 2*N*(2+(N-1));

  int i = 1;
  while (i <= n) {
    printf("*", (total_n - i) % 10);
    i++;
  }
  printf("\n");
  int printf_count = 0;
  int count = n;
  while (count < n*n) {
    int x = count % n;
    int y = count / n;
    int val = 0;

    int temp_x = n-1;
    int temp_y = 0;

    int down = n - 1;
    int left = n - 1;
    int up = n - 3;
    int right = n - 3;

    int total_count = 0;
    int found = 0;
    while ((right > 0 || left > 0) && !found) { // finish looping

      int down_count = 0;
      while (down_count < down) { // down
        down_count++;
        temp_y++;
        if (temp_x == x && temp_y == y) {
          val = (total_count + down_count); // FOUND!
          found = 1;
          break;
        }
      }
      total_count += down_count;
      down -= 4;

      int left_count = 0;
      while (left_count < left) { // left
        left_count++;
        temp_x--;
        if (temp_x == x && temp_y == y) {
          val = (total_count + left_count); // FOUND!
          found = 1;
          break;
        }
      }
      total_count += left_count;
      left -= 4;

      int up_count = 0;
      while (up_count < up) { // up
        up_count++;
        temp_y--;
        if (temp_x == x && temp_y == y) {
          val = (total_count + up_count); // FOUND!
          found = 1;
          break;
        }
      }
      total_count += up_count;
      up -= 4;

      int right_count = 0;
      while (right_count < right) { // left
        right_count++;
        temp_x++;
        if (temp_x == x && temp_y == y) {
          val = (total_count + right_count); // FOUND!
          found = 1;
          break;
        }
      }
      total_count += right_count;
      right -= 4;
    }
    if (!found) {
      printf("-");
    } else {
      printf("*",(total_n - (n + val)) % 10);
      printf_count++;
    }
    if (x == n-1) {
      printf("\n");
    }
    count++;
  }

  return 0;
}
