// Written by Jeremie Kull
// on 18-3-2018
// A program to print factors and determine if it's a perfect number
// 17monsitar

#include <math.h>
#include <stdio.h>

int factors(int n) {
  int count = 1;
  int sum = 0;
  printf("The factors of %d are:\n", n);
  while (count <= n) {
    if (!(n % count)) {
      printf("%d\n", count);
      sum += count;
    }
    count++;
  }
  return sum;
}

int main(void) {
  printf("Enter number: ");
  int num;
  scanf("%d", &num);

  int sum = factors(num);
  printf("Sum of factors = %d\n", sum);

  if (sum - num == num) {
    printf("%d is a perfect number\n", num);
  } else {
    printf("%d is not a perfect number\n", num);
  }

  return 0;
}
