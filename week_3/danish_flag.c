// Written by Jeremie Kull
// on 18-3-2018
// A program to a danish flag
// 17monsitar

#include <stdio.h>

int main(void) {
  printf("Enter the flag size: ");
  int size;
  scanf("%d", &size);

  int y = 0;
  while (y < size*4) {
    int x = 0;
    while (x < size*9) {
      if (x == size*3 - 1 || x == size*3 || y == size*2 - 1 || y == size*2) {
        printf(" ");
      } else {
        printf("#");
      }
      x++;
    }
    y++;
    printf("\n");
  }

  return 0;
}
