// Written by Jeremie Kull
// on 18-3-2018
// A program to countdown from 10
// 17monsitar

#include <stdlib.h>
#include <stdio.h>

int main(void) {
  int count = 10;
  while (count >= 0) {
    printf("%d\n", count);
    count--;
  }

  return 0;
}
