// Written by Jeremie Kull
// on 18-3-2018
// A program to print variable sized X's
// 17monsitar

#include <math.h>
#include <stdio.h>

int main(void) {
  printf("Enter size: ");
  int size;
  scanf("%d", &size);

  int y = 0;
  while (y < size) {
    int x = 0;
    while (x < size) {
      if (x==y || x == size - y - 1) {
        printf("*");
      } else {
        printf("-");
      }
      x++;
    }
    y++;
    printf("\n");
  }

  return 0;
}
