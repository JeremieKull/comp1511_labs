// This program takes in your age, and considers the range you should be dating.
// DATE: 05/03/18
// Author: Michael Lloyd, Jeremie Kull


#include <stdio.h>
#include <stdlib.h>


int main(void) {

	int age, lowBound, upBound;
	printf("Enter your age: ");
	scanf("%d", &age);
	lowBound = (age / 2) + 7;
	upBound = (age - 7) * 2;

	if(lowBound >= age) {
		printf("You are too young to be dating.\n");
	}

	else {
		printf("Your dating range is %d to %d years old.\n", lowBound, upBound);
	}


	return 0;
}
