// Adds two integers together, and prints the process and sum.
// DATE: 05/04/2018
// AUTHOR(S): Michael Lloyd, Jeremie Kull

#include <stdlib.h>
#include <stdio.h>

int main(void) {
	int num1, num2;
	printf("Please enter two integers: ");
	scanf("%d %d", &num1, &num2);

	printf("%d + %d = %d\n", num1, num2, num1 + num2);

	return 0;
}
