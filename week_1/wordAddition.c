// The program converts numbered inputs into words and then "adds" them together.
// AUTHOR: Michael Lloyd
// DATE: 05/04/2018

#include <stdlib.h>
#include <stdio.h>

int main(void) {
	int inputOne, inputTwo;
	printf("Please enter two integers: ");
	scanf("%d %d", &inputOne, &inputTwo);	
	int sum = inputOne + inputTwo;

	//Checks if number needs to be printed as number or word
	//If printed as a word, identifies the word and prints.

	if(inputOne > 10 || inputOne < (-10)) {
		printf("%d + ", inputOne);
	}
	else {
		if(inputOne == 0){
			printf("zero + ");
		}
		if(inputOne < 0) {
			if(inputOne == -1){
				printf("negative one + ");
			}
			if(inputOne == -2){
				printf("negative two + ");
			}
			if(inputOne == -3){
				printf("negative three + ");
			}
			if(inputOne == -4){
				printf("negative four + ");
			}
			if(inputOne == -5){
				printf("negative five + ");
			}
			if(inputOne == -6){
				printf("negative six + ");
			}
			if(inputOne == -7){
				printf("negative sevem + ");
			}
			if(inputOne == -8){
				printf("negative eight + ");
			}
			if(inputOne == -9){
				printf("negative nine + ");
			}
			if(inputOne == -10){
				printf("negative ten + ");
			}
		}
		if(inputOne > 0) {
			if(inputOne == 1){
				printf("one + ");
			}
			if(inputOne == 2){
				printf("two + ");
			}
			if(inputOne == 3){
				printf("three + ");
			}
			if(inputOne == 4){
				printf("four + ");
			}
			if(inputOne == 5){
				printf("five + ");
			}
			if(inputOne == 6){
				printf("six + ");
			}
			if(inputOne == 7){
				printf("seven + ");
			}
			if(inputOne == 8){
				printf("eight + ");
			}
			if(inputOne == 9){
				printf("nine + ");
			}
			if(inputOne == 10){
				printf("ten + ");
			}
		}
	}
	
	if(inputTwo > 10 || inputTwo < (-10)) {
		printf("%d = ", inputTwo);
	}
	else {
		if(inputTwo == 0) {
			printf("zero = ");
		}
		if(inputTwo < 0) {
			if(inputTwo == -1){
				printf("negative one = ");
			}
			if(inputTwo == -2){
				printf("negative two = ");
			}
			if(inputTwo == -3){
				printf("negative three = ");
			}
			if(inputTwo == -4){
				printf("negative four = ");
			}
			if(inputTwo == -5){
				printf("negative five = ");
			}
			if(inputTwo == -6){
				printf("negative six = ");
			}
			if(inputTwo == -7){
				printf("negative seven = ");
			}
			if(inputTwo == -8){
				printf("negative eight = ");
			}
			if(inputTwo == -9){
				printf("negative nine = ");
			}
			if(inputTwo == -10){
				printf("negative ten = ");
			}
		}
		if(inputTwo > 0) {
			if(inputTwo == 1){
				printf("one = ");
			}
			if(inputTwo == 2){
				printf("two = ");
			}
			if(inputTwo == 3){
				printf("three = ");
			}
			if(inputTwo == 4){
				printf("four = ");
			}
			if(inputTwo == 5){
				printf("five = ");
			}
			if(inputTwo == 6){
				printf("six = ");
			}
			if(inputTwo == 7){
				printf("seven = ");
			}
			if(inputTwo == 8){
				printf("eight = ");
			}
			if(inputTwo == 9){
				printf("nine = ");
			}
			if(inputTwo == 10){
				printf("ten = ");
			}
		}
	}
	
	//Checks if the sum needs to be expressed in words
	//If not, identifies number as word, and prints.

	if(sum > 10 || sum < (-10)){
		printf("%d", sum);
	}
	else {
		if(sum == 0){
			printf("%d", sum);
		}
		if(sum < 0) {
			if(sum == -1){
				printf("negative one");
			}
			if(sum == -2){
				printf("negative two");
			}
			if(sum == -3){
				printf("negative three");
			}
			if(sum == -4){
				printf("negative four");
			}
			if(sum == -5){
				printf("negative five");
			}
			if(sum == -6){
				printf("negative six");
			}
			if(sum == -7){
				printf("negative seven");
			}
			if(sum == -8){
				printf("negative eight");
			}
			if(sum == -9){
				printf("negative nine");
			}
			if(sum == -10){
				printf("negative ten");
			}
		}
		if(sum > 0) {
			if(sum == 1){
				printf("one");
			}
			if(sum == 2){
				printf("two");
			}
			if(sum == 3){
				printf("three");
			}
			if(sum == 4){
				printf("four");
			}
			if(sum == 5){
				printf("five");
			}
			if(sum == 6){
				printf("six");
			}
			if(sum == 7){
				printf("seven");
			}
			if(sum == 8){
				printf("eight");
			}
			if(sum == 9){
				printf("nine");
			}
			if(inputTwo == 10){
				printf("ten");
			}
		}	
	}
	printf("\n");
	return 0;
		}
