//This program helps find out how many scoops of ice-cream Matilda can buy.
// DATE: 05/04/2018
// AUTHOR(S): Michael Lloyd, Jeremie Kull


#include <stdlib.h>
#include <stdio.h>

int main(void) {

	int cost, scoops;

	printf("How many scoops? ");
	scanf("%d", &scoops);

	printf("How many dollars does each scoop cost? ");
	scanf("%d", &cost);

	if ((scoops * cost) > 10) {
		printf("Oh no, you don't have enough money :(\n");
	} 
	else {
		printf("You have enough money!\n");
	}

	return 0;
}
