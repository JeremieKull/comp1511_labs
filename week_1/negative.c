// This program indicates if your number is negative or positive. 
// DATE: 05/04/2018
// AUTHOR: Michael Lloyd, Jeremie Kull


#include <stdlib.h>
#include <stdio.h>

int main(void) {
	int userNumber;

	scanf("%d", &userNumber);

	if (userNumber < 0) {
		printf("Don't be so negative!\n");
	} 
	else if (userNumber > 0) {
		printf("You have entered a positive number.\n");
	} 
	else {
		printf("You have entered zero.\n");
	}

	return 0;
}
