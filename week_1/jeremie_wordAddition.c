// Written by Jeremie Kull
// on 6-3-2018
// A program to print a word sum of numbers

#include <stdlib.h>
#include <stdio.h>

int main(void) {
	int num1, num2;
	printf("Please enter two integers: ");
	scanf("%d %d", &num1, &num2);
	int sum = num1 + num2;

	// First number negative
	if (num1 < 0) {
		printf("negative ");
		num1 *= -1;
	}

	int testNum;
	if (num1 < 11 && num1 > -11) {
		// First num to words
		testNum = num1;
		if (testNum == 0) {
			printf("zero ");
		} else if (testNum == 1) {
			printf("one ");
		} else if (testNum == 2) {
			printf("two ");
		} else if (testNum == 3) {
			printf("three ");
		} else if (testNum == 4) {
			printf("four ");
		} else if (testNum == 5) {
			printf("five ");
		} else if (testNum == 6) {
			printf("six ");
		} else if (testNum == 7) {
			printf("seven ");
		} else if (testNum == 8) {
			printf("eight ");
		} else if (testNum == 9) {
			printf("nine ");
		} else if (testNum == 10) {
			printf("ten ");
		}
	} else {
		printf("%d ", num1);
	}

	printf("+ ");

	// Second number negative
	if (num2 < 0) {
		printf("negative ");
		num2 *= -1;
	}

	// Second num to words
	if (num2 < 11 && num2 > -11) {
		testNum = num2;
		if (testNum == 0) {
			printf("zero ");
		} else if (testNum == 1) {
			printf("one ");
		} else if (testNum == 2) {
			printf("two ");
		} else if (testNum == 3) {
			printf("three ");
		} else if (testNum == 4) {
			printf("four ");
		} else if (testNum == 5) {
			printf("five ");
		} else if (testNum == 6) {
			printf("six ");
		} else if (testNum == 7) {
			printf("seven ");
		} else if (testNum == 8) {
			printf("eight ");
		} else if (testNum == 9) {
			printf("nine ");
		} else if (testNum == 10) {
			printf("ten ");
		}
	} else {
		printf("%d ", num2);
	}

	printf("= ");

	// Sum number negative
	if (sum < 0) {
		printf("negative ");
		sum *= -1;
	}

	if (sum < 11 && sum > -11) {
		// sum num to words
		testNum = sum;
		if (testNum == 0) {
			printf("zero");
		} else if (testNum == 1) {
			printf("one");
		} else if (testNum == 2) {
			printf("two");
		} else if (testNum == 3) {
			printf("three");
		} else if (testNum == 4) {
			printf("four");
		} else if (testNum == 5) {
			printf("five");
		} else if (testNum == 6) {
			printf("six");
		} else if (testNum == 7) {
			printf("seven");
		} else if (testNum == 8) {
			printf("eight");
		} else if (testNum == 9) {
			printf("nine");
		} else if (testNum == 10) {
			printf("ten");
		}
	} else {
		printf("%d", sum);
	}

	printf("\n");

	return 0;
}
