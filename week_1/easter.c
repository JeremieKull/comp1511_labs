// Calculates the Date of Easter in a year 
// DATE: 05/04/2018
// AUTHOR: Michael Lloyd

#include <stdio.h>
#include <stdlib.h>

int main(void){
	int a, b, c, d, e, f, g, h, i, k, l, m, easterMonth, easterDate, p, year;

	printf("Enter Year: ");
	scanf("%d", &year);

	a = year % 19;
	b = year / 100;
	c = year % 100;
	d = b / 4;
	e = b % 4;
	f = (b + 8) / 25;
	g = (b - f + 1) / 3;
	h = (19 * a + b - d - g + 15) % 30;
	i = c / 4;
	k = c % 4;
	l = (32 + 2 * e + 2 * i - h - k) % 7;
	m = (a + 11 * h + 22 * l) / 451;
	easterMonth = (h + l - 7 * m + 114) / 31; // (January = 1)
	p = (h + l - 7 * m + 114) % 31;
	easterDate = p + 1;

	if(easterMonth == 1) {
			printf("Easter is January %d in %d.\n", easterDate, year);
	}

	else if(easterMonth == 2) {
		printf("Easter is February %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 3) {
		printf("Easter is March %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 4) {
		printf("Easter is April %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 5) {
		printf("Easter is May %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 6) {
		printf("Easter is June %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 7) {
		printf("Easter is July %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 8) {
		printf("Easter is August %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 9) {
		printf("Easter is September %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 10) {
		printf("Easter is October %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 11) {
		printf("Easter is November %d in %d.\n", easterDate, year);
	}
	

	else if(easterMonth == 12) {
		printf("Easter is December %d in %d.\n", easterDate, year);
	}
	
	return 0;

}
