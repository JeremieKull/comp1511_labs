// The program converts numbered inputs into words and then "adds" them together.
// AUTHOR: Michael Lloyd
// DATE: 05/04/2018

#include <stdlib.h>
#include <stdio.h>

int main(void) {
	int inputOne, inputTwo;
 	char *stringOne, *stringTwo;
	printf("Please enter two integers: ");
	scanf("%d %d", &inputOne, &inputTwo);
	

	char *index[21] = {"negative ten", "negative nine", "negative eight", "negative seven", "negative six", "negative five",
	"negative four", "negative three", "negative two", "negative one", "zero", "one", "two", "three", "four", "five",
	"six", "seven", "eight", "nine", "ten"};
	
	//Sort and identify negatives or Zeroes
	if(inputOne > 10 || inputOne < (-10) || inputTwo > 10 || inputTwo < (-10)) {
		printf("%d + %d = %d\n", inputOne, inputTwo, inputOne + inputTwo);
	}
	else {
	if(inputOne <= 0 || inputTwo <= 0){
		if(inputOne < 0){
			stringOne = index[10+inputOne];
		}
		else if(inputOne == 0){
			stringOne = index[10];
		}
		if(inputTwo < 0) {
			stringTwo = index[10+inputTwo];
		}
		else if(inputTwo == 0) {
			stringTwo = index[10+inputTwo];
		}
	}

	//Sort and identify positive
	if(inputOne > 0 || inputTwo > 0){
		if(inputOne > 0){
			stringOne = index[10+inputOne];
		}
		if(inputTwo > 0){
			stringTwo = index[10+inputTwo];
		}
	}
	
	//Check to see if the result is in numbers or words
	if(inputOne + inputTwo < 10 && inputOne + inputTwo > (-10)) {
		printf("%s + %s = %s\n", stringOne, stringTwo, index[10+(inputOne + inputTwo)]);
	}
	else {
	printf("%s + %s = %d\n", stringOne, stringTwo, inputOne+inputTwo);
	}
	}

	 return 0;
}
